/*
===============================
1. Luby Transform (LT) Codes
===============================

• The idea of LT codes is that the sender is a fountain that produces an 
endless supply of encoded packets, say the original source file has a size 
of Kl bits, and each packet contains l encoded bits.

• Anyone who wishes to receive the complete file holds a bucket under 
the fountain and collects packets until they have collected a little more 
than K (in practice, this is usually around 5%). They can then almost 
certainly (with prob. 1 − δ) recover the original file exactly.

• LT codes are rateless in the sense that the number of encoded packets 
that can be generated from the source message is potentially limitless 
and the number of encoded packets generated can be determined on the fly.

• Regardless of the statistics of the erasure events on the channel, we 
can send as many encoded packets as are needed in order for the decoder to 
recover the source data.

• LT codes also have fantastically small encoding and decoding complexities. 
With probability 1 − δ, K packets can be communicated with average encoding 
and decoding costs both of order K ln(K/δ) packet operations.

• Luby calls these codes universal because they are simultaneously near 
optimal for every erasure channel, and they are very efficient as the file 
length K grows [4].

===============================
2. Task
===============================
• The task is to implement two programs. 
• The first, an encoder, reads a file and generates another file with blocks 
encoded by an LT Code.
• The second, a decoder, reads one such encoded file and either successfully 
decodes it, or says that it has insufficient info to decode.

[
• The first, an encoder, reads a file and generates an infinite stream of 
blocks encoded by an LT Code. 
• The second, a decoder, reads such a stream until it is possible to 
reconstruct the original file.
]

• LT Codes depend on randomness for their implementation, and the HOWTO to 
generate the required (pseudo-)random numbers so that the encoder and decoder 
will work deterministically, given proper pseudo-random seeds are already 
stated in [1].

===============================
2.1 Algorithm
===============================
• LT Codes comprise two main algorithms, one for encoding and another for 
decoding. For more detail, refer to [2].


• Consider a source file with K fixed-length blocks s k , k = 1, . . . , K. 
• We assume a degree distribution μ(d) is provided, which is a discrete 
probability distribution (probability mass function) on integers between 1 and K:
           K
μ(d) ≥ 0, ∑ μ(d) = 1. 
          d=1 
• Each encoded packet t n in the digital fountain is then produced as follows:
1. Randomly sample the degree d n of the packet from μ(d).
2. Choose, uniformly at random, d n distinct input blocks (packets). 
	Set t n equal to the bitwise sum (modulo 2) of these d n blocks.
	(Computed by successively XOR-ing the packets together. This is equivalent 
	to the bitwise XOR operation, denoted ⊕, on the blocks)

• The encoded message is then these encoded packets, plus sufficient info
for the decoder to determine which source blocks where combined to produce 
each packet.

(NOTE: This encoding operation defines a graph connecting encoded packets to 
source packets. If the mean degree d is significantly smaller than K then 
the graph is sparse)



• Now suppose that N encoded packets t1 . . . t N have been successfully 
received.
• For each packet t n , construct a list of the source blocks s k which were 
used to encode that packet. 
• The decoder then proceeds as follows:
1. Find a packet t n which has exactly one source block s k in its list. 
	If no such packet exists, the decoder halts and fails. Otherwise:
	(a) Set s k = t n .
	(b) Set t n′ = t n′ ⊕ s k , for all packets t n′ which include 
		source block s k in their encoding lists.
	(c) Delete source block s k from all encoding lists.
2. Repeat step 1 until all source blocks are decoded.


• As discussed by MacKay [2], it may be helpful to visualize the decoder 
using a sparse bipartite graph, in which edges show which source blocks are 
encoded by each packet. 
• For those who are curious, this decoder is a special case of the celebrated 
sum-product or loopy belief propagation (BP) algorithm. 
• Because there can be no errors in received packets, only complete erasures, 
the general BP algorithm substantially simplifies for LT codes.

===============================
2.2 Robust Soliton Distribution
===============================

• While the encoder and decoder in Sec. 2.1 are valid algorithms for any 
degree distribution, the decoder only succeeds with high probability if μ(d) 
is chosen with care.

• A starting point is the ideal soliton distribution:
ρ(1) = 1/K, ρ(d) = 1/d(d-1)		for d = 2, 3, . . . , K.		(1)

(NOTE: The expected average degree under this distribution is roughly: ln(K))

• This distribution optimizes the expected probability that there is one 
decodable source block at each iteration, but has an unacceptably high 
probability of failing at some iteration.

• To add robustness, Luby defines the following non-negative function:
	τ(d) =(S/K)(1/d)			for d = 1, 2, . . . , ⌊K/S⌋ − 1,
	τ(d) =(S/K)ln(S/δ)			for d = ⌊K/S⌋,
	τ(d) = 0					for d > ⌊K/S⌋,

	S = c ln(K/δ)√K

(NOTE: S: the expected number of degree-one check node (through out decoding 
process))

• Here, 0 < δ < 1 is a (conservative) bound on the probability that the 
decoding fails to succeed after a certain number of packets are received. 

• c > 0 is a free parameter, which can be tuned to optimize performance.

• The robust soliton distribution is:
	                                	K
	μ(d) = ( ρ(d) + τ(d) )/Z, where Z = ∑ ρ(d) + τ(d)			(2)
	                               		d=1

(NOTE: Receiver once receives K' = KZ encoded packets ensures that the 
decoding can run to completion with probability at least 1 - δ.)

• The inclusion of Z creates a properly normalized distribution which sums 
to one.

• The robust soliton distribution of Eq. (2) defines the distribution μ(d) 
which will used when implementing the encoder. 

• To sample from μ(d), first compute the corresponding cumulative distribution 
function:
    	    d
	M(d) = ∑ μ(d ′ ) 										(3)
    	    d ′ =1

• Let u denote a number uniformly distributed between 0 and 1, for example 
drawn from the pseudo-random generator of Sec. 2.3. 

• We can then construct a sample d from μ(d) by finding the unique bin 
(degree) for which M(d − 1) ≤ u < M(d), where M(0) = 0.

• For this assignment, we will fix the parameters for the distribution [1]. 
We use the values of c = 0.1 and δ = 0.5.

*/


/*
=======================================
2.3 Pseudo-Random Number Generation
=======================================
• We will use a very simple pseudo-random generator, a variant of a linear 
congruential generator, known as the Lehmer generator. 

• With the particular parameters specified below, it is called MinStd [3]. 

• The generator is defined by the following equation:
	next = A ⋅ state mod M						(4)

• We will use A = 16807 and M = 2^31 − 1 = 2147483647. 
M is a Mersenne prime, and A is a primitive root modulo M, which guarantees 
maximum period for the random sequence. 

• We define three operations on a generator R:

1. R.nextInt(): returns next, and sets state = next.
2. R.setSeed(S): sets state = S.
3. R.getState(): returns state.

• A snippet of C code that implements nextInt() observing the width of 
the data types:
*/

uint32_t M = 2147483647UL; //Mersenne prime
uint32_t A = 16807; //primitive root modulo M
uint32_t MAX_RAND = M - 1;
uint32_t state;

uint32_t nextInt() {
	uint32_t next = (uint32_t)(((uint64_t)state * A) % M);
	state = next;
	return next;
}

/*
• To produce a number uniformly distributed between 0 and 1, which is needed 
for generating samples from μ(d), double precision should be used and the 
obtained integer should be divided by M−1 = MAX_RAND, defined above.

• We have provided a sequence of samples from this random number generator 
in Appendix A [1], which can be used as an indication that our generator is 
producing correct samples.
*/

/*
=======================================
2.4 Encoding the List of Blocks
=======================================

• One important aspect of the decoder is that it needs to know, 
for each encoded packet, the number and identity of the source blocks 
from which it was created. 

• Instead of encoding the list explicitly in the packet, which could be 
wasteful, we will have the decoder generate this list using the same process 
as the one used by the encoder. 

• Since this involves sequences of (pseudo-)random numbers, we will have to 
make sure the programs generate the same sequence for each block.

• We will store in the encoded block the internal state of the random 
generator immediately before encoding the block. 

• This state, for our generator from Sec. 2.3, is simply a 32-bit number, 
which we call the seed for the block. 

• Given this seed, we will follow the steps below for the block.
 
• Since the state of the generator changes with each invocation, 
it is important to follow these steps exactly:
1. Before processing block t n :
	(a) If encoding t n , t n .seed = R.getState()
	(b) If decoding t n , R.setSeed(t n .seed)
2. Generate r = R.nextInt() and use it to generate d from the robust soliton 
	distribution (see Sec. 2.2).
3. Generate d distinct numbers between 0 and K − 1, using 
	(R.nextInt() mod K) for each one. In case of repetition, keep generating 
	new numbers until you get d distinct source blocks. This is the list of 
	source blocks corresponding to this encoded block.

• Note that according to this, the source blocks are numbered 0 to K − 1.
• Appendix B [1] has a list of blocks generated in sequence with a fixed seed 
so we can compare our program.

=======================================
2.5 Programs
=======================================

• We will write two executable console programs: encode and decode.

• encode will receive the block size, a random seed, a rate, and 
the name of a file to encode. 

[
• encode will receive the name of a file to encode, the block size, 
and optionally a random seed.
]

• Assume the file will be in the same directory as the program, to simplify 
handling of the name. 

• encode will be called as follows:
$> encode <block_size> <seed> <rate> <file>

[
$ encode <file> <block-size> [<seed>]
]

• Where:
block size		is an integer, the size of each encoded block, in bytes.
seed 			is an integer, the initial seed for the random number generator.
rate 			is a real, > 1, controlling the number of encoded blocks
				generated. If there are K source blocks, the program should 
				generate r ⋅ K encoded blocks.
file			is the name of the source file to be encoded.

• If the name of the input file is file, encode will create an output file 
named file.lt. We describe the format of the file in Sec. 2.6.

[
• encode will process the file into blocks and then continually stream 
blocks to stdout. 
• We describe the format to stream these blocks in Sec. 2.6. 
• encode must be able to handle files whose size is not a multiple of 
the block size.
]


• decode will receive the name of an encoded file, which will have all of 
the necessary information to decode it. Section 2.6 describes the file 
format in detail. 

• It will be invoked as follows:
$> decode <file>

• The output of decode will be one of the two options, depending on 
whether it successfully decodes the file:
Successfully decoded <file>.lt into <file>.lt.dec or 
Failed to decode <file>.lt

• In case of successful decoding, the produced file must be identical to 
the original file. 




• In particular, care should be taken to truncate the last block of the file 
if the file size is not a multiple of the block size.

• You don’t have to worry about malformed encoded files. 

• You also don’t have to worry that the files used for testing won’t fit 
in memory, i.e., you may assume that the decoder, for example, can hold 
the contents of the encoded and decoded blocks in memory. 

• With that said, you should make sure that you can decode at least a 100MB file.

• You can choose to have the decoder read all encoded blocks before 
starting to decode, or start decoding as it reads the blocks. 

• Bear in mind, however, that for the evaluation (see Sec. 3.3), 
you will need to know the minimum number of encoded blocks needed to 
successfully decode a file.


[
• decode will receive the name of an encoded file, which will have all of 
the necessary information to decode it. 
• decode will receive an optional drop rate, and will receive a stream of 
blocks on stdin.
• For any given block, it will drop the block (that is, neglect to process 
the block and simply move on to reading the next block) with probability 
given by the drop rate. 
• Once enough blocks have been received to successfully reconstruct 
the original file, it will write this file to stdout. 
• It will be invoked as follows:
$ decode [<drop-rate>]
]

*/


/*
=======================================
2.6 Data Format
=======================================
• The remaining aspect that we need to specify is the on-disk format 
for the encoded files. 
• The file is to be written in binary format, not in text format. 
• Integers in the file are to be written in big-endian format, 
also known as network byte order. 
• This means that the integer 0x01020304 will be written to the file as 
the sequence of bytes 0x01, 0x02, 0x03, and 0x04. 
• This convention is important for compatibility between your programs and ours, 
regardless of the language you write them in.
• For Java, the methods DataOuput.writeInt and DataInput.readInt will do the 
right thing. 
• For C and C++, you will need to use the conversion functions htonl before 
writing, and ntohl after reading. 
• Other languages have similar mechanisms.
• Note that this only applies to integers, and byte sequences are written 
in the same order as they are in memory.
• The file format is simple: there is a header which has the parameters for 
the decoder program, followed by a sequence of encoded blocks. 
• The following C-like listing describes the format:
*/

typedef struct { //endianness marker
	uint32_t 0x01020304; //block size (bytes)
	uint32_t B; //number of encoded blocks
	uint32_t E; //size of original file (bytes)
	uint32_t F; //number of source blocks
	uint32_t K; 
} header_t; 

typedef struct { //seed for the block
	uint32_t seed; //block data
	uint8_t data[B]; 
} encoded_block_t; 

typedef struct {
	header_t header;
	encoded_block_t blocks[E];
} encoded_file_t;

/*
• Use the first integer to make sure you are writing the integers correctly. 
• The first byte of the file will always be 0x01, followed by 0x02, 0x03, 
and 0x04. You can use the hexdump program to verify this, if you want.
*/


/*[]

• The remaining aspect that we need to specify is the wire format for the blocks. 
• The data in the blocks are to be written verbatim, and the fields in the block 
header are to be written in network byte order.
• The block format is simple: there is a header which specifies the file size and 
the block size (in case this is the first block received by the encoder - remember, 
the encoder doesn’t know ahead of time anything about the file it’s receiving). 
• It also specifies the seed for the random number generator so that the decoder 
can reconstruct the list of blocks that this block corresponds to. The data directly 
follows the header. The format is as follows:

Block:
	uint32_t fileSize;
	uint32_t blockSize;
	uint32_t blockSeed;
	char data[blockSize];

• Note that fileSize may not necessarily be a multiple of blockSize. 
• Since all blocks are the same size, this means that the final block may extend 
beyond the end of the file. 
• In this case, it doesn’t matter what data is stored in this part of the block, 
but it is vitally important that it be constant - if the data changes over time, 
it invalidates the decoding scheme (since the data in the final block may be used 
to XOR with other blocks). 
• Given that the decoder knows the file size from the block header, it is possible 
for the decoder to know where the actual file ends even if it does not end on 
a block boundary. 
• Though it would technically be possible to decode with a scheme that allows for 
arbitrarily many junk blocks, it would be wasteful, and so we require that, if the 
file size is not a multiple of the block size, only the final block contain junk data, 
and it is a multiple, there is no junk data (and thus there are no extra blocks).
*/

/*
References:
[1] Capstone Project: LT Codes, 
Programming Comps - January 17-24, 2012 LT Erasure Codes
[2] D.J.C. MacKay. Information theory, inference, and learning algorithms. 
Cambridge Univ Pr, 2003.
[3] S. K. Park and K. W. Miller. Random number generators: good ones are 
hard to find. Commun. ACM, 31:1192–1201, October 1988.
[4] CSC310 – Information Theory by Sam Roweis.
Lecture 21: Erasure (Deletion) Channels & Digital Fountain Codes, 
November 22, 2006
[5] LT-Codes in Java by zemasa
[6] LT-Codes in Python by anrosent
[7] Implementation of Luby transform Code in C# by Omar Gameel Salem,
28 Nov 2014


*/


