/*
***************************************************************
* Robust Soliton Distribution (RSD) implementation in C++
* as described in 2.2 for testing.
* The ideal soliton distribution (rho) is:
* 	ρ(1) = 1/K, ρ(d) = 1/d(d-1)		for d = 2, 3, . . . , K.
* To add robustness, the following non-negative function is defined (tau):
* 	τ(d) =(S/K)(1/d)				for d = 1, 2, . . . , ⌊K/S⌋ - 1,
* 	τ(d) =(S/K)ln(S/δ)				for d = ⌊K/S⌋,
* 	τ(d) = 0						for d > ⌊K/S⌋,
* 	S = c ln(K/δ)√K
* The RSD is:
* 	                        			      K
* 	μ(d) = ( ρ(d) + τ(d) )/Z,		where Z = ∑  ρ(d) + τ(d)
* 	                         				  d=1
***************************************************************
*/

#include <iostream>
#include <cmath> //log, sqrt, floor
#include <cstdlib> //random, srandom
#include <cstdio>
#include <getopt.h>//
#include<cstring>

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

#define M 2147483647UL //Mersenne prime: 2^31-1 i.e (1 << 31)-1
#define A 16807 //primitive root modulo M: 7^5
#define MAX_RAND 2147483646UL

typedef unsigned long uint32_t;
typedef unsigned long long uint64_t;
typedef char uint8_t;

static uint32_t state; //state of the linear PRNG

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

//The ideal soliton distribution
double rho(uint32_t i, uint32_t K)
{
	//TODO: check if (i<1 || i> K)
	if (i==1) {
		return(1.0/K);
	} else {
		return(1.0/(i*(i-1)));
	}
}

//The extra part for the RSD
double tau(uint32_t i, uint32_t K, double S, uint32_t KS, double delta)
{
	//TODO: check if (i<1 || i> K)
	if (i < KS) {
		return((S/K)*(1.0/i));
	} else {
		if (i > KS) {
			return(0);
		} else {
			return((S/K)*log(S/delta));
		}
	}
}

//The RSD
double mu(uint32_t i, uint32_t K, double S, uint32_t KS,
												double delta, double Z)
{
	return((rho(i, K)+tau(i, K, S, KS, delta))/Z);
}

////////////////////////////////////////////////////////////////////////

//The linear PRNG: MINSTD
inline uint32_t nextInt()
{
	uint32_t next = (uint32_t)(((uint64_t)state * A) % M);
	state = next;
	return next;
}

//Sets state
void setSeed(uint32_t seed)
{
	printf ("seed=%ld\n", seed);
	state = seed;
}

//Return state
uint32_t getState()
{
	return state;
}

////////////////////////////////////////////////////////////////////////

//Calculates the number of encoded packets required at the receiving end
uint32_t getBlocksNeeded(uint32_t K, double Z)
{
	return (uint32_t) (K*Z);
}

////////////////////////////////////////////////////////////////////////

// Samples degree from the CDF of mu
uint32_t sample(uint32_t K, double S, uint32_t KS, double delta, double Z)
{
	std::cout << "state/blockseed=" << getState() << std::endl;
    uint32_t n = nextInt();
	double p = n /(double)MAX_RAND;
	std::cout << "p=" << p << " state=" << getState() << std::endl;
	uint32_t i;
	double sum=0;
	for(i=1; i<=K; i++) {
		sum+=mu(i, K, S, KS, delta, Z);
		if (sum > p) {
			return (i);
		}
	}
	return (i);
}

////////////////////////////////////////////////////////////////////////

//Check whether an element is in an array
int isInArray(uint32_t val, uint32_t *arr, uint32_t size)
{
    uint32_t i;
    for (i=0; i < size; i++) {
        if (arr[i] == val)
            return (0);
    }
    return(1);
}

void getSrcBlocks(uint32_t seed, uint32_t *blockseed, uint32_t *d,
												uint32_t *nums,
												uint32_t size,
												uint32_t K,
												double S,
												uint32_t KS,
												double delta,
												double Z)
{
	uint32_t i;

	if (seed > 0) {
		setSeed(seed);
	}

	*blockseed = getState();
	//Samples degree
	*d = sample(K, S, KS, delta, Z);

	for (i=0; i < *d; i++) {
		uint32_t num = nextInt() % K;
		if (isInArray(num, nums, *d)) {
			nums[i] = num;
		}
	}
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////


int main(int argc, char* argv[])
{
    //FILE *f_in; //*f_out;
    //struct pcap_file_header pfh;
    //struct pcap_pkthdr pkh;

	std::cout.precision(16);

	uint32_t K;//Number of blocks to be encoded
	double c; //Free parameter
	double delta; //Admissible failure probability
	double S; //Parameter used for the tau function
	uint32_t KS; //floor of K/S
	double Z; //Normalization factor for the RSD
	uint32_t i; //index

	K = 10000;
	c = 0.2;
	delta = 0.05;
	Z = 0.0;

    /////////////////////////////////////////////////////////////////////
	//Print out our config parameters
	std::cout << "K=" << K << " c=" << c << " delta=" << delta << std::endl;

	//Set the seed for our PRNG
	setSeed(2067261);

	//Test our PRNG
	for (i=0; i <= (14*7); i++) {
		nextInt();
		printf ("%ld\t", getState());
		if (i%7 == 0)
			printf("\n");
	}
	printf("\n");


	//Calculate S
	S = c * log(K/delta) * sqrt(K);
	std::cout << "S=" << S << std::endl;

	//Calculate floor of K/S (⌊K/S⌋)
	KS = (uint32_t) floor(K/S);
	std::cout << "KS=" << KS << std::endl;

	//Calculate Z, i.e: Z = ∑  ρ(i) + τ(i)
	//                     i=1
	for(i=1; i<=K; i++) {
		Z += rho(i, K) + tau(i, K, S, KS, delta);
		//XXX Uncomment this for checking
		//std::cout << "i " << i<< " rho=" << rho(i, K) << " tau=" << tau(i, K, S, KS, delta) << std::endl;
	}
	std::cout << "Z=" << Z << std::endl;

/*
	//Calculate the CDF of the RSD
	double sum;
	sum = 0.0;
	for(i=1; i<=K; i++) {
		sum+=mu(i, K, S, KS, delta, Z);
		//XXX Uncomment this for checking
		//std::cout << "i " << i <<" " << sum << std::endl;
	}
*/

    /////////////////////////////////////////////////////////////////////
/*
	//XXX Uncomment this for checking
	uint32_t blockseed;
	uint32_t d;
	uint32_t nums[K];

	//d = sample(K, S, KS, delta, Z);
	//std::cout << "sample=" << d << std::endl;

    while (1) {
		//blockseed, d, ix_samples = getSrcBlocks();
		getSrcBlocks(0,&blockseed, &d, nums, sizeof(nums), K, S, KS, delta, Z);
		//block_data = 0
		//for ix in ix_samples:
		//    block_data ^= blocks[ix] //d!
		std::cout << "blockseed=" << blockseed << " d=" << d << std::endl;
		std::cout << "{ ";
		for (i=0; i<d;i++){
			std::cout << " " << nums[i] << "\t";
		}
		std::cout << "}\n";
	}
*/
	return 0;
}

