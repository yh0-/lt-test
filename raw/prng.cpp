/*
* A very simple pseudo-random generator implementation in C++
as described in 2.3 Pseudo-Random Number Generation

*/

//#include <iostream>
//#include <cmath> //log, sqrt, cos, sin
//#include <cstdlib> //random, srandom
#include <cstdio>

typedef unsigned long uint32_t;
typedef unsigned long long uint64_t;

class MyPrng {
public:
	MyPrng() {
		init();
	}

	void init() {
		M = 2147483647UL; //Mersenne prime
		A = 16807; //primitive root modulo M
	}

	uint32_t nextInt() {
		uint32_t next = (uint32_t)(((uint64_t)state * A) % M);
		state = next;
		return next;
	}

	void setSeed(uint32_t seed) {
		printf ("seed=%ld\n", seed);
		state = seed;
	}

	uint32_t getState() {
		return state;
	}

private:
	uint32_t M;//Mersenne prime
	uint32_t A;//primitive root modulo M //	uint32_t MAX_RAND = M - 1;
	uint32_t state;
};

int main(int argc, char* argv[])
{
	int i;
	MyPrng rng;
	rng.setSeed(2067261);
	for (i=0; i <= (14*7); i++) {
		rng.nextInt();
		printf ("%ld\t", rng.getState());
		if (i%7 == 0)
			printf("\n");
	}
	printf("\n");
	return 0;
}

